﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ArabicSupport;
using DG.Tweening;

public class GameManager : MonoBehaviour
{
    [Header("Lock Screen References")]
    public Transform LockHolder;
    public Text ArabTitle;
    public Text EngTitle;
    public Text ButtonText;
    public Button PlayButton;


    [Header("Game References")]
    public Transform GameHolder;
    public Text Title;
    public Text InfoTitle;
    public Text InfoDisc;
    public Text OreginalInfo;
    public Image OreginalInfoCot1;
    public Image OreginalInfoCot2;

    public Text EngInfoTitle;
    public Text EngInfoDisc;
    public Text EngOreginalInfo;
    public Image EngOreginalInfoCot1;
    public Image EngOreginalInfoCot2;

    public Button BackButn;
    public Button LangBtn;
    public Transform LangTgl;
    public Image LangTxt;
    public Transform RocksButtonsHolder;
    public Button[] Rocks;
    public Text[] RocksNames;
    public AudioSource AS;
    public Image LoadingScreen;

    [Space]

    public Image EmptyBar;
    public Image FillingBar;
    public Text timeTxt;
    public Button PlayAudioButton;

    [Space]

    [Header("Changing References")]
    public Sprite[] colordRocksSprite;
    public Sprite[] NormalRocksSprite;
    public GameObject[] PlayingRocksSprite;
    public Sprite playBTn;
    public Sprite pauseBTN;

    [Space]
    public bool isEnglish;

    public Sprite EngBtn;
    public Sprite AraBtn;

    [Space]

    public int currentOpenedRock;
    public bool soundIsPlaying;
    public int time = 0;

    [Header("Rocks Posisions when a rock opend")]
    public Vector3[] GreeckPosesions;
    public Vector3[] Greeck2Posesions;
    public Vector3[] ArabicPosesions;
    public Vector3[] NabateenPosesions;
    public Vector3[] SafawiPosesions;
    public Vector3[] MsmariPosesions;
    public Vector3[] UntranslatedPosesions;
    public Vector3[] LatienPosesions;

    private bool playBTNanim;
    Coroutine swtchlng;
    [Space]

    [Header("Game Data")]
    public string[] ArabicRocksTitles;
    public string[] EngleshRocksTitles;
    [Space]
    public string[] ArabicRocksDisc;
    public string[] EngleshRocksDisc;
    [Space]
    public string[] ArabicRocksOri;
    public string[] EngleshRocksOri;
    // Use this for initialization
    [Space]
    public AudioClip[] ArabicRocksSounds;
    public AudioClip[] EngleshRocksSounds;

    void Start()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        StartGame();
    }

    public void StartGame()
    {
        PlayButton.image.DOFade(0.0f, 0.5f).SetEase(Ease.OutCubic).SetDelay(0.25f);
        PlayButton.transform.DOScale(0.5f, 0.75f).SetEase(Ease.InOutBack);
        ButtonText.DOFade(0.0f, 0.5f).SetEase(Ease.OutCubic).SetDelay(0.25f);
        ButtonText.transform.DOScale(0.5f, 0.75f).SetEase(Ease.InCubic);

        ArabTitle.DOFade(0.0f, 1.0f).SetEase(Ease.OutCubic).SetDelay(0.35f);
        ArabTitle.transform.DOScale(0.8f, 0.750f).SetEase(Ease.InCubic).SetDelay(0.35f);
        EngTitle.DOFade(0.0f, 1.0f).SetEase(Ease.OutCubic).SetDelay(0.35f);
        EngTitle.transform.DOScale(0.8f, 0.750f).SetEase(Ease.InCubic).SetDelay(0.35f);

        Title.transform.DOScale(1, 0.75f).SetDelay(0.7f);
        Title.DOFade(1, 0.750f).SetEase(Ease.InCubic).SetDelay(0.7f);

        for (int i = 0; i < Rocks.Length; i++)
        {
            int x = i;
            RocksNames[i].transform.DOScale(1, 0.750f).SetDelay(1);
            RocksNames[i].DOFade(1, 0.750f).SetEase(Ease.InCubic).SetDelay(1).OnComplete(() =>
            {
                Rocks[x].interactable = true;
            });
        }

        LangBtn.interactable = true;

    }

    public void OpenRockNo(int no)
    {
        if (currentOpenedRock == -1)
        {
            currentOpenedRock = no;
            if (no != 7)
            {
                Rocks[no].transform.SetParent(GameHolder);
            }
            RocksButtonsHolder.DOScale(0.85f, 0.5f).SetEase(Ease.InCubic);

            if (no != 0)
            {
                Rocks[0].transform.DOLocalMove(GreeckPosesions[no], 0.5f).SetEase(Ease.InCubic);
            }
            if (no != 1)
            {
                Rocks[1].transform.DOLocalMove(Greeck2Posesions[no], 0.5f).SetEase(Ease.InCubic);
            }
            if (no != 2)
            {
                Rocks[2].transform.DOLocalMove(ArabicPosesions[no], 0.5f).SetEase(Ease.InCubic);
            }
            if (no != 3)
            {
                Rocks[3].transform.DOLocalMove(NabateenPosesions[no], 0.5f).SetEase(Ease.InCubic);
            }
            if (no != 4)
            {
                Rocks[4].transform.DOLocalMove(SafawiPosesions[no], 0.5f).SetEase(Ease.InCubic);
            }
            if (no != 5)
            {
                Rocks[5].transform.DOLocalMove(MsmariPosesions[no], 0.5f).SetEase(Ease.InCubic);
            }
            if (no != 6)
            {
                Rocks[6].transform.DOLocalMove(UntranslatedPosesions[no], 0.5f).SetEase(Ease.InCubic);
            }
            if (no != 7)
            {
                Rocks[7].transform.DOLocalMove(LatienPosesions[no], 0.5f).SetEase(Ease.InCubic);
            }
            for (int i = 0; i < Rocks.Length; i++)
            {
                //Rocks[i].interactable = false;
                if (i == no)
                {
                    continue;
                }
                RocksNames[i].DOFade(0.4f, 0.5f);

            }
            Rocks[no].image.sprite = colordRocksSprite[no];
            if (isEnglish)
            {
                EngInfoTitle.text = EngleshRocksTitles[no];
                EngInfoDisc.text = EngleshRocksDisc[no];
                EngOreginalInfo.text = EngleshRocksOri[no];



                EngInfoTitle.DOFade(1.0f, 0.75f).SetEase(Ease.InCubic).SetDelay(0.3f);
                EngInfoTitle.transform.DOScale(1.0f, 0.5f).SetEase(Ease.InCubic).SetDelay(0.3f);

                EngInfoDisc.DOFade(1.0f, 0.75f).SetEase(Ease.InCubic).SetDelay(0.5f);
                EngInfoDisc.transform.DOScale(1.0f, 0.5f).SetEase(Ease.InCubic).SetDelay(0.5f);

                if (EngOreginalInfo.text != "")
                {
                    EngOreginalInfo.DOFade(1.0f, 0.75f).SetEase(Ease.InCubic).SetDelay(0.7f);
                    EngOreginalInfo.transform.DOScale(1.0f, 0.5f).SetEase(Ease.InCubic).SetDelay(0.7f);
                    EngOreginalInfoCot1.DOFade(1.0f, 0.75f).SetEase(Ease.InCubic).SetDelay(0.7f);
                    EngOreginalInfoCot2.DOFade(1.0f, 0.75f).SetEase(Ease.InCubic).SetDelay(0.7f);
                }
            }
            else
            {
                InfoTitle.text = ArabicRocksTitles[no];
                InfoDisc.text = ArabicRocksDisc[no];
                OreginalInfo.text = ArabicRocksOri[no];

                InfoTitle.DOFade(1.0f, 0.75f).SetEase(Ease.InCubic).SetDelay(0.3f);
                InfoTitle.transform.DOScale(1.0f, 0.5f).SetEase(Ease.InCubic).SetDelay(0.3f);

                InfoDisc.DOFade(1.0f, 0.75f).SetEase(Ease.InCubic).SetDelay(0.5f);
                InfoDisc.transform.DOScale(1.0f, 0.5f).SetEase(Ease.InCubic).SetDelay(0.5f);

                if (OreginalInfo.text != "")
                {
                    OreginalInfo.DOFade(1.0f, 0.75f).SetEase(Ease.InCubic).SetDelay(0.7f);
                    OreginalInfo.transform.DOScale(1.0f, 0.5f).SetEase(Ease.InCubic).SetDelay(0.7f);
                    OreginalInfoCot1.DOFade(1.0f, 0.75f).SetEase(Ease.InCubic).SetDelay(0.7f);
                    OreginalInfoCot2.DOFade(1.0f, 0.75f).SetEase(Ease.InCubic).SetDelay(0.7f);
                }
            }

            Title.transform.DOScale(0.8f, 0.75f);
            Title.DOFade(0, 0.750f).SetEase(Ease.InCubic);



            EmptyBar.DOFade(1.0f, 0.75f).SetEase(Ease.InCubic).SetDelay(0.9f);
            FillingBar.DOFade(1.0f, 0.75f).SetEase(Ease.InCubic).SetDelay(0.9f);
            timeTxt.DOFade(1.0f, 0.75f).SetEase(Ease.InCubic).SetDelay(0.9f);
            PlayAudioButton.image.DOFade(1.0f, 0.75f).SetEase(Ease.InCubic).SetDelay(0.9f).OnComplete(() =>
            {
                PlayAudioButton.interactable = true;
                BackButn.gameObject.SetActive(true);
                LangBtn.interactable = true;
            });
            Invoke("PlayTheAudio", 2.0f);
        }
        else /// يعني فاتح وحدة منهم
        {
            //Debug.Log("Switch"); 
            LoadingScreen.gameObject.SetActive(true);
            LoadingScreen.DOFade(0.75f, 0.3f);
            BackButn.gameObject.SetActive(false);
            LangBtn.interactable = false;

            StopCoroutine("PlayingAudioSeq");
            PlayingRocksSprite[currentOpenedRock].SetActive(false);

            FillingBar.fillAmount = 0;
            time = 0;
            AS.clip = null;
            AS.Stop();
            soundIsPlaying = false;
            PlayAudioButton.interactable = false;
            EmptyBar.DOFade(0.0f, 0.75f).SetEase(Ease.InCubic);
            FillingBar.DOFade(0.0f, 0.75f).SetEase(Ease.InCubic);
            timeTxt.DOFade(0.0f, 0.75f).SetEase(Ease.InCubic);
            PlayAudioButton.image.DOFade(0.0f, 0.75f).SetEase(Ease.InCubic);

            InfoTitle.DOFade(0.0f, 0.75f).SetEase(Ease.InCubic).SetDelay(0.3f);
            InfoTitle.transform.DOScale(0.8f, 0.5f).SetEase(Ease.InCubic).SetDelay(0.3f);

            InfoDisc.DOFade(0.0f, 0.75f).SetEase(Ease.InCubic).SetDelay(0.5f);
            InfoDisc.transform.DOScale(0.8f, 0.5f).SetEase(Ease.InCubic).SetDelay(0.5f);

            OreginalInfo.DOFade(0.0f, 0.75f).SetEase(Ease.InCubic).SetDelay(0.7f);
            OreginalInfo.transform.DOScale(0.8f, 0.5f).SetEase(Ease.InCubic).SetDelay(0.7f);
            OreginalInfoCot1.DOFade(0.0f, 0.75f).SetEase(Ease.InCubic).SetDelay(0.7f);
            OreginalInfoCot2.DOFade(0.0f, 0.75f).SetEase(Ease.InCubic).SetDelay(0.7f);

            EngInfoTitle.DOFade(0.0f, 0.75f).SetEase(Ease.InCubic).SetDelay(0.3f);
            EngInfoTitle.transform.DOScale(0.8f, 0.5f).SetEase(Ease.InCubic).SetDelay(0.3f);
            EngInfoDisc.DOFade(0.0f, 0.75f).SetEase(Ease.InCubic).SetDelay(0.5f);
            EngInfoDisc.transform.DOScale(0.8f, 0.5f).SetEase(Ease.InCubic).SetDelay(0.5f);
            EngOreginalInfo.DOFade(0.0f, 0.75f).SetEase(Ease.InCubic).SetDelay(0.7f);
            EngOreginalInfo.transform.DOScale(0.8f, 0.5f).SetEase(Ease.InCubic).SetDelay(0.7f);
            EngOreginalInfoCot1.DOFade(0.0f, 0.75f).SetEase(Ease.InCubic).SetDelay(0.7f);
            EngOreginalInfoCot2.DOFade(0.0f, 0.75f).SetEase(Ease.InCubic).SetDelay(0.7f).OnComplete(() =>
            {
                if (isEnglish)
                {
                    EngInfoTitle.text = EngleshRocksTitles[no];
                    EngInfoDisc.text = EngleshRocksDisc[no];
                    EngOreginalInfo.text = EngleshRocksOri[no];



                    EngInfoTitle.DOFade(1.0f, 0.75f).SetEase(Ease.InCubic).SetDelay(1.3f);
                    EngInfoTitle.transform.DOScale(1.0f, 0.5f).SetEase(Ease.InCubic).SetDelay(1.3f);

                    EngInfoDisc.DOFade(1.0f, 0.75f).SetEase(Ease.InCubic).SetDelay(1.5f);
                    EngInfoDisc.transform.DOScale(1.0f, 0.5f).SetEase(Ease.InCubic).SetDelay(1.5f);

                    if (EngOreginalInfo.text != "")
                    {
                        EngOreginalInfo.DOFade(1.0f, 0.75f).SetEase(Ease.InCubic).SetDelay(1.7f);
                        EngOreginalInfo.transform.DOScale(1.0f, 0.5f).SetEase(Ease.InCubic).SetDelay(1.7f);
                        EngOreginalInfoCot1.DOFade(1.0f, 0.75f).SetEase(Ease.InCubic).SetDelay(1.7f);
                        EngOreginalInfoCot2.DOFade(1.0f, 0.75f).SetEase(Ease.InCubic).SetDelay(1.7f);
                    }
                }
                else
                {
                    InfoTitle.text = ArabicRocksTitles[no];
                    InfoDisc.text = ArabicRocksDisc[no];
                    OreginalInfo.text = ArabicRocksOri[no];

                    InfoTitle.DOFade(1.0f, 0.75f).SetEase(Ease.InCubic).SetDelay(0.3f);
                    InfoTitle.transform.DOScale(1.0f, 0.5f).SetEase(Ease.InCubic).SetDelay(0.3f);

                    InfoDisc.DOFade(1.0f, 0.75f).SetEase(Ease.InCubic).SetDelay(0.5f);
                    InfoDisc.transform.DOScale(1.0f, 0.5f).SetEase(Ease.InCubic).SetDelay(0.5f);

                    if (OreginalInfo.text != "")
                    {
                        OreginalInfo.DOFade(1.0f, 0.75f).SetEase(Ease.InCubic).SetDelay(0.7f);
                        OreginalInfo.transform.DOScale(1.0f, 0.5f).SetEase(Ease.InCubic).SetDelay(0.7f);
                        OreginalInfoCot1.DOFade(1.0f, 0.75f).SetEase(Ease.InCubic).SetDelay(0.7f);
                        OreginalInfoCot2.DOFade(1.0f, 0.75f).SetEase(Ease.InCubic).SetDelay(0.7f);
                    }
                }
                LoadingScreen.DOFade(0.0f, 0.3f).OnComplete(() =>
                {
                    LoadingScreen.gameObject.SetActive(false);
                });
            });


            Rocks[currentOpenedRock].transform.SetParent(RocksButtonsHolder);
            Rocks[currentOpenedRock].transform.localScale = Vector3.one;
            Rocks[currentOpenedRock].image.sprite = NormalRocksSprite[currentOpenedRock];


            currentOpenedRock = no;
            if (no != 7)
            {
                Rocks[no].transform.SetParent(GameHolder);
            }

            //if (no != 0)
            //{
            //    Rocks[0].transform.DOLocalMove(GreeckPosesions[no], 0.5f).SetEase(Ease.InCubic);
            //}
            //if (no != 1)
            //{
            //    Rocks[1].transform.DOLocalMove(Greeck2Posesions[no], 0.5f).SetEase(Ease.InCubic);
            //}
            //if (no != 2)
            //{
            //    Rocks[2].transform.DOLocalMove(ArabicPosesions[no], 0.5f).SetEase(Ease.InCubic);
            //}
            //if (no != 3)
            //{
            //    Rocks[3].transform.DOLocalMove(NabateenPosesions[no], 0.5f).SetEase(Ease.InCubic);
            //}
            //if (no != 4)
            //{
            //    Rocks[4].transform.DOLocalMove(SafawiPosesions[no], 0.5f).SetEase(Ease.InCubic);
            //}
            //if (no != 5)
            //{
            //    Rocks[5].transform.DOLocalMove(MsmariPosesions[no], 0.5f).SetEase(Ease.InCubic);
            //}
            //if (no != 6)
            //{
            //    Rocks[6].transform.DOLocalMove(UntranslatedPosesions[no], 0.5f).SetEase(Ease.InCubic);
            //}
            //if (no != 7)
            //{
            //    Rocks[7].transform.DOLocalMove(LatienPosesions[no], 0.5f).SetEase(Ease.InCubic);
            //}
            for (int i = 0; i < Rocks.Length; i++)
            {
                //Rocks[i].interactable = false;
                if (i == no)
                {
                    continue;
                }
                RocksNames[i].DOFade(0.4f, 0.5f);
            }

            Rocks[no].image.sprite = colordRocksSprite[no];




            EmptyBar.DOFade(1.0f, 0.75f).SetEase(Ease.InCubic).SetDelay(0.9f);
            FillingBar.DOFade(1.0f, 0.75f).SetEase(Ease.InCubic).SetDelay(0.9f);
            timeTxt.DOFade(1.0f, 0.75f).SetEase(Ease.InCubic).SetDelay(0.9f);
            PlayAudioButton.image.DOFade(1.0f, 0.75f).SetEase(Ease.InCubic).SetDelay(0.9f).OnComplete(() =>
            {
                PlayAudioButton.interactable = true;
                BackButn.gameObject.SetActive(true);
                LangBtn.interactable = true;
            });
            Invoke("PlayTheAudio", 2.0f);

        }

    }

    public void ExitRocks()
    {
        BackButn.gameObject.SetActive(false);
        LangBtn.interactable = false;

        StopCoroutine("PlayingAudioSeq");
        PlayingRocksSprite[currentOpenedRock].SetActive(false);


        FillingBar.fillAmount = 0;
        time = 0;
        AS.clip = null;
        AS.Stop();
        soundIsPlaying = false;
        PlayAudioButton.interactable = false;
        EmptyBar.DOFade(0.0f, 0.75f).SetEase(Ease.InCubic);
        FillingBar.DOFade(0.0f, 0.75f).SetEase(Ease.InCubic);
        timeTxt.DOFade(0.0f, 0.75f).SetEase(Ease.InCubic);
        PlayAudioButton.image.DOFade(0.0f, 0.75f).SetEase(Ease.InCubic);


        InfoTitle.DOFade(0.0f, 0.75f).SetEase(Ease.InCubic).SetDelay(0.3f);
        InfoTitle.transform.DOScale(0.8f, 0.5f).SetEase(Ease.InCubic).SetDelay(0.3f);

        InfoDisc.DOFade(0.0f, 0.75f).SetEase(Ease.InCubic).SetDelay(0.5f);
        InfoDisc.transform.DOScale(0.8f, 0.5f).SetEase(Ease.InCubic).SetDelay(0.5f);

        OreginalInfo.DOFade(0.0f, 0.75f).SetEase(Ease.InCubic).SetDelay(0.7f);
        OreginalInfo.transform.DOScale(0.8f, 0.5f).SetEase(Ease.InCubic).SetDelay(0.7f);
        OreginalInfoCot1.DOFade(0.0f, 0.75f).SetEase(Ease.InCubic).SetDelay(0.7f);
        OreginalInfoCot2.DOFade(0.0f, 0.75f).SetEase(Ease.InCubic).SetDelay(0.7f);

        EngInfoTitle.DOFade(0.0f, 0.75f).SetEase(Ease.InCubic).SetDelay(0.3f);
        EngInfoTitle.transform.DOScale(0.8f, 0.5f).SetEase(Ease.InCubic).SetDelay(0.3f);
        EngInfoDisc.DOFade(0.0f, 0.75f).SetEase(Ease.InCubic).SetDelay(0.5f);
        EngInfoDisc.transform.DOScale(0.8f, 0.5f).SetEase(Ease.InCubic).SetDelay(0.5f);
        EngOreginalInfo.DOFade(0.0f, 0.75f).SetEase(Ease.InCubic).SetDelay(0.7f);
        EngOreginalInfo.transform.DOScale(0.8f, 0.5f).SetEase(Ease.InCubic).SetDelay(0.7f);
        EngOreginalInfoCot1.DOFade(0.0f, 0.75f).SetEase(Ease.InCubic).SetDelay(0.7f);
        EngOreginalInfoCot2.DOFade(0.0f, 0.75f).SetEase(Ease.InCubic).SetDelay(0.7f);

        Title.transform.DOScale(1.0f, 0.75f).SetDelay(0.9f);
        Title.DOFade(1, 0.750f).SetEase(Ease.InCubic).SetDelay(0.9f);

        //if (isEnglish)
        //{
        //    InfoTitle.text = EngleshRocksTitles[currentOpenedRock];
        //    InfoDisc.text = EngleshRocksDisc[currentOpenedRock];
        //    OreginalInfo.text = EngleshRocksOri[currentOpenedRock];
        //}
        //else
        //{
        //    InfoTitle.text = ArabicRocksTitles[currentOpenedRock];
        //    InfoDisc.text = ArabicRocksDisc[currentOpenedRock];
        //    OreginalInfo.text = ArabicRocksOri[currentOpenedRock];
        //}

        if (currentOpenedRock != 0)
        {
            Rocks[0].transform.DOLocalMove(GreeckPosesions[0], 0.5f).SetEase(Ease.InCubic).SetDelay(0.9f);
        }
        if (currentOpenedRock != 1)
        {
            Rocks[1].transform.DOLocalMove(Greeck2Posesions[1], 0.5f).SetEase(Ease.InCubic).SetDelay(0.9f);
        }
        if (currentOpenedRock != 2)
        {
            Rocks[2].transform.DOLocalMove(ArabicPosesions[2], 0.5f).SetEase(Ease.InCubic).SetDelay(0.9f);
        }
        if (currentOpenedRock != 3)
        {
            Rocks[3].transform.DOLocalMove(NabateenPosesions[3], 0.5f).SetEase(Ease.InCubic).SetDelay(0.9f);
        }
        if (currentOpenedRock != 4)
        {
            Rocks[4].transform.DOLocalMove(SafawiPosesions[4], 0.5f).SetEase(Ease.InCubic).SetDelay(0.9f);
        }
        if (currentOpenedRock != 5)
        {
            Rocks[5].transform.DOLocalMove(MsmariPosesions[5], 0.5f).SetEase(Ease.InCubic).SetDelay(0.9f);
        }
        if (currentOpenedRock != 6)
        {
            Rocks[6].transform.DOLocalMove(UntranslatedPosesions[6], 0.5f).SetEase(Ease.InCubic).SetDelay(0.9f);
        }
        if (currentOpenedRock != 7)
        {
            Rocks[7].transform.DOLocalMove(LatienPosesions[7], 0.5f).SetEase(Ease.InCubic).SetDelay(0.9f);
        }

        RocksButtonsHolder.DOScale(1.0f, 0.5f).SetEase(Ease.InCubic).OnComplete(() =>
        {
            Rocks[currentOpenedRock].transform.SetParent(RocksButtonsHolder);
            Rocks[currentOpenedRock].image.sprite = NormalRocksSprite[currentOpenedRock];
            for (int i = 0; i < Rocks.Length; i++)
            {
                Rocks[i].interactable = true;
                RocksNames[i].DOFade(1.0f, 0.5f);
                LangBtn.interactable = true;
            }
            currentOpenedRock = -1;
        }).SetDelay(0.9f);


    }

    public void PlayTheAudio()
    {
        if (!playBTNanim)
        {
            playBTNanim = true;
            PlayAudioButton.transform.DOScale(1.2f, 0.25f).OnComplete(() =>
            {
                PlayAudioButton.transform.DOScale(1.0f, 0.25f).OnComplete(() =>
                {
                    playBTNanim = false;
                });
            });
            if (!soundIsPlaying)
            {
                soundIsPlaying = true;
                PlayAudioButton.image.sprite = pauseBTN;
                StartCoroutine("PlayingAudioSeq");
            }
            else
            {
                soundIsPlaying = false;
                PlayAudioButton.image.sprite = playBTn;
            }
        }

    }

    IEnumerator PlayingAudioSeq()
    {

        if (time == 0)
        {
            if (isEnglish)
            {
                //AS.clip = EngleshRocksSounds[currentOpenedRock];
                AS.clip = EngleshRocksSounds[currentOpenedRock];

            }
            else
            {
                AS.clip = ArabicRocksSounds[currentOpenedRock];
            }
            AS.Play();
        }
        else
        {
            AS.UnPause();
        }

        int lngth = Mathf.CeilToInt(ArabicRocksSounds[currentOpenedRock].length);
        int minC = 0;
        int secC = 0;
        while (soundIsPlaying && time != lngth)
        {
            yield return new WaitForSeconds(1.0f);
            if (currentOpenedRock != -1)
            {
                if (PlayingRocksSprite[currentOpenedRock])
                {
                    PlayingRocksSprite[currentOpenedRock].SetActive(true);
                }
            }

            time++;
            FillingBar.fillAmount = ((float)time / (float)lngth);
            minC = time / 60;
            secC = (time % 60);// * 60;
            string ses = "" + secC;
            string mis = "" + minC;
            if (secC < 10)
            {
                ses = "0" + secC;
            }
            else
            {
                ses = "" + secC;

            }
            if (minC < 10)
            {
                mis = "0" + minC;
            }
            else
            {
                mis = "" + minC;
            }
            timeTxt.text = mis + ":" + ses;

        }
        PlayingRocksSprite[currentOpenedRock].SetActive(false);
        AS.Pause();
        if (time == lngth)
        {
            time = 0;
            AS.Stop();
        }
    }

    public void SwitchLanguage()
    {
        if (swtchlng == null)
        {
            swtchlng = StartCoroutine(SwitchLanguageSeq());
        }

    }

    void FadeOutTexts(bool smallTitles, bool largeTitles, bool Disc, bool ori)
    {
        Title.DOFade(0, 0.750f).SetEase(Ease.InCubic);
        if (smallTitles)
        {
            for (int i = 0; i < RocksNames.Length; i++)
            {
                int x = i;
                RocksNames[i].transform.DOScale(0.8f, 0.750f);//.SetDelay(1);
                RocksNames[i].DOFade(0, 0.750f).SetEase(Ease.InCubic)/*.SetDelay(1)*/.OnComplete(() =>
                {
                    Rocks[x].interactable = false;
                });
            }
        }
        if (largeTitles)
        {
            InfoTitle.transform.DOScale(0.8f, 0.750f);//.SetDelay(1);
            InfoTitle.DOFade(0, 0.750f).SetEase(Ease.InCubic);//.SetDelay(1);
            EngInfoTitle.transform.DOScale(0.8f, 0.750f);//.SetDelay(1);
            EngInfoTitle.DOFade(0, 0.750f).SetEase(Ease.InCubic);//.SetDelay(1);
        }
        if (Disc)
        {
            InfoDisc.transform.DOScale(0.8f, 0.750f);//.SetDelay(1);
            InfoDisc.DOFade(0, 0.750f).SetEase(Ease.InCubic);//.SetDelay(1);            
            EngInfoDisc.transform.DOScale(0.8f, 0.750f);//.SetDelay(1);
            EngInfoDisc.DOFade(0, 0.750f).SetEase(Ease.InCubic);//.SetDelay(1);
        }
        if (ori)
        {
            OreginalInfo.transform.DOScale(0.8f, 0.750f);//.SetDelay(1);
            OreginalInfo.DOFade(0, 0.750f).SetEase(Ease.InCubic);//.SetDelay(1);
            OreginalInfoCot1.DOFade(0, 0.750f).SetEase(Ease.InCubic);
            OreginalInfoCot2.DOFade(0, 0.750f).SetEase(Ease.InCubic);

            EngOreginalInfo.transform.DOScale(0.8f, 0.750f);//.SetDelay(1);
            EngOreginalInfo.DOFade(0, 0.750f).SetEase(Ease.InCubic);//.SetDelay(1);
            EngOreginalInfoCot1.DOFade(0, 0.750f).SetEase(Ease.InCubic);
            EngOreginalInfoCot2.DOFade(0, 0.750f).SetEase(Ease.InCubic);
        }
    }

    void FadeInTexts(bool smallTitles, bool largeTitles, bool Disc, bool ori)
    {
        Title.DOFade(1, 0.750f).SetEase(Ease.InCubic);
        if (smallTitles)
        {
            for (int i = 0; i < RocksNames.Length; i++)
            {
                int x = i;
                RocksNames[i].transform.DOScale(1.0f, 0.750f);//.SetDelay(1);
                RocksNames[i].DOFade(1, 0.750f).SetEase(Ease.InCubic)/*.SetDelay(1)*/.OnComplete(() =>
                {
                    Rocks[x].interactable = transform;
                });
            }
        }
        if (largeTitles)
        {
            if (isEnglish)
            {
                EngInfoTitle.transform.DOScale(1.0f, 0.750f);//.SetDelay(1);
                EngInfoTitle.DOFade(1.0f, 0.750f).SetEase(Ease.InCubic);//.SetDelay(1);
            }
            else
            {
                InfoTitle.transform.DOScale(1.0f, 0.750f);//.SetDelay(1);
                InfoTitle.DOFade(1.0f, 0.750f).SetEase(Ease.InCubic);//.SetDelay(1);
            }

        }
        if (Disc)
        {
            if (isEnglish)
            {
                EngInfoDisc.transform.DOScale(1.0f, 0.750f);//.SetDelay(1);
                EngInfoDisc.DOFade(1.0f, 0.750f).SetEase(Ease.InCubic);//.SetDelay(1);
            }
            else
            {
                InfoDisc.transform.DOScale(1.0f, 0.750f);//.SetDelay(1);
                InfoDisc.DOFade(1.0f, 0.750f).SetEase(Ease.InCubic);//.SetDelay(1);
            }

        }
        if (ori)
        {
            if (isEnglish)
            {
                EngOreginalInfo.transform.DOScale(1.0f, 0.750f);//.SetDelay(1);
                EngOreginalInfo.DOFade(1.0f, 0.750f).SetEase(Ease.InCubic);//.SetDelay(1);
                EngOreginalInfoCot1.DOFade(1.0f, 0.750f).SetEase(Ease.InCubic);
                EngOreginalInfoCot2.DOFade(1.0f, 0.750f).SetEase(Ease.InCubic);
            }
            else
            {
                OreginalInfo.transform.DOScale(1.0f, 0.750f);//.SetDelay(1);
                OreginalInfo.DOFade(1.0f, 0.750f).SetEase(Ease.InCubic);//.SetDelay(1);
                OreginalInfoCot1.DOFade(1.0f, 0.750f).SetEase(Ease.InCubic);
                OreginalInfoCot2.DOFade(1.0f, 0.750f).SetEase(Ease.InCubic);
            }
        }
    }

    void FadeInAllTexts()
    {
        for (int i = 0; i < RocksNames.Length; i++)
        {
            int x = i;
            RocksNames[i].transform.DOScale(1.0f, 0.750f);//.SetDelay(1);
            RocksNames[i].DOFade(1, 0.750f).SetEase(Ease.InCubic)/*.SetDelay(1)*/.OnComplete(() =>
            {
                Rocks[x].interactable = transform;
            });
        }
        InfoTitle.transform.DOScale(1.0f, 0.750f);//.SetDelay(1);
        InfoTitle.DOFade(1.0f, 0.750f).SetEase(Ease.InCubic);//.SetDelay(1);
        InfoDisc.transform.DOScale(1.0f, 0.750f);//.SetDelay(1);
        InfoDisc.DOFade(1.0f, 0.750f).SetEase(Ease.InCubic);//.SetDelay(1);
        OreginalInfo.transform.DOScale(1.0f, 0.750f);//.SetDelay(1);
        OreginalInfo.DOFade(1.0f, 0.750f).SetEase(Ease.InCubic);//.SetDelay(1);
        OreginalInfoCot1.DOFade(1.0f, 0.750f).SetEase(Ease.InCubic);//.SetDelay(1);
        OreginalInfoCot2.DOFade(1.0f, 0.750f).SetEase(Ease.InCubic);//.SetDelay(1);
    }

    IEnumerator SwitchLanguageSeq()
    {

        //\/\/\/\/\/\/\/\/\/\/TEMP\/\/\/\/\/\/\/\/\/\/\\
        LoadingScreen.gameObject.SetActive(true);
        LoadingScreen.DOFade(0.85f, 0.3f);
        LangBtn.interactable = false;
        int temprock = currentOpenedRock;
        if (isEnglish)
        {
            isEnglish = false;

            if (currentOpenedRock != -1)
            {
                ExitRocks();
            }
            //////////togglr/////////
            LangTxt.DOFade(0.0f, 0.5f);
            LangTxt.transform.DOScale(0.5f, 0.5f);
            LangTgl.DOLocalMoveX(-LangTgl.localPosition.x, 1.0f).SetEase(Ease.InOutCubic);
            yield return new WaitForSeconds(0.5f);
            LangTxt.transform.DOLocalMoveX(-LangTxt.transform.localPosition.x, 0.0f);
            LangTxt.sprite = EngBtn;
            LangTxt.transform.DOScale(1.0f, 0.5f);
            LangTxt.DOFade(1.0f, 0.5f);
            //////////togglr/////////

            yield return new WaitForSeconds(0.75f);
            FadeOutTexts(true, true, true, true);
            yield return new WaitForSeconds(1.0f);
            Title.text = "النقر للاستماع للنقوش";

            //EngInfoTitle.alignment = TextAnchor.MiddleRight;
            //EngInfoDisc.alignment = TextAnchor.UpperRight;
            //EngOreginalInfo.alignment = TextAnchor.UpperRight;
            for (int i = 0; i < RocksNames.Length; i++)
            {
                RocksNames[i].text = ArabicRocksTitles[i];
            }

            if (currentOpenedRock == -1)
            {
                FadeInTexts(true, false, false, false);
            }
            else
            {
                FadeInTexts(true, true, true, true);

            }
            yield return new WaitForSeconds(1.0f);
        }
        else
        {
            isEnglish = true;
            //int temprock = currentOpenedRock;
            if (currentOpenedRock != -1)
            {
                ExitRocks();
            }

            LangTxt.DOFade(0.0f, 0.5f);
            LangTxt.transform.DOScale(0.5f, 0.5f);
            LangTgl.DOLocalMoveX(-LangTgl.localPosition.x, 1.0f).SetEase(Ease.InOutCubic);
            yield return new WaitForSeconds(0.5f);
            LangTxt.transform.DOLocalMoveX(-LangTxt.transform.localPosition.x, 0.0f);
            LangTxt.sprite = AraBtn;
            LangTxt.transform.DOScale(1.3f, 0.5f);
            LangTxt.DOFade(1.0f, 0.5f);

            yield return new WaitForSeconds(0.75f);
            FadeOutTexts(true, true, true, true);
            yield return new WaitForSeconds(1.0f);
            Title.text = "Click to listen to the Inscriptions";

            //InfoTitle.alignment = TextAnchor.MiddleLeft;
            //InfoDisc.alignment = TextAnchor.UpperLeft;
            //OreginalInfo.alignment = TextAnchor.UpperLeft;
            for (int i = 0; i < RocksNames.Length; i++)
            {
                RocksNames[i].text = EngleshRocksTitles[i];
            }

            if (currentOpenedRock == -1)
            {
                FadeInTexts(true, false, false, false);
            }
            else
            {
                FadeInTexts(true, true, true, true);

            }
            yield return new WaitForSeconds(1.0f);
        }
        if (temprock != -1)
        {
            OpenRockNo(temprock);
        }
        LoadingScreen.DOFade(0.0f, 0.3f);
        yield return new WaitForSeconds(0.4f);
        LoadingScreen.gameObject.SetActive(false);
        LangBtn.interactable = true;
        swtchlng = null;
    }

    public void PrintPoss()
    {
        if (currentOpenedRock != -1)
        {
            Debug.Log("<b>Opened Rock is: " + Rocks[currentOpenedRock].gameObject.name + "</b>");
        }

        for (int i = 0; i < Rocks.Length; i++)
        {
            if (i == currentOpenedRock)
            {
                continue;
            }
            Debug.Log(Rocks[i].gameObject.name + ": " + Rocks[i].transform.localPosition);
        }
    }
}
